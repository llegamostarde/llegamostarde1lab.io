---
title: "#14 Linux Express ¿Mp3 o Ogg?"
author: juan
date: 2017-05-20
category: linuxexpress
featimg: 2017/14LinuxExpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/%2314%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2017/14LinuxExpress.png)  
Me acabo de levantar con una idea. ¿Y si produzco mis episodios en formato libre? El mp3 no pasa por sus mejores momentos
y creo que va siendo hora de darle una oportunidad al .ogg.
<audio controls>
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/%2314%20Podcast%20Linux%20Express.ogg" type="audio/mpeg">
Your browser does not support the audio element.
</audio>



Si no puedes reproducir este episodio es que es incompatible en tu podcastcher. Tanto si sí o si no, te agradecería tu feedback
en Twitter <https://twitter.com/podcastlinux>

Tengo ganas de saber cómo sale este experiemento.

Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://feedpress.me/podcastlinux>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.gitlab.io/Linux-Express/feed.xml>
