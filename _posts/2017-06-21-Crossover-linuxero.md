---
title: "Crossover Linuxero KilallRadio"
author: juan
date: 2017-06-21
category: linuxexpress
featimg: 2017/crossover.png
podcast:
  audio: https://www.ivoox.com/20-crossover-linuxero-directo-sobre-gnome-plasma_md_19395669_wp_1
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2017/crossover.png)  
Hoy traigo un nuevo podcast, un crossover que realizamos Yoyo, Elav, Paco Estrada, Dj Mao Mix y Richie... y el querido perro de mi vecino... Risas y más risas y una charla interesante con muy buenos amigos de Tux.
<audio controls>
  <source src="https://www.ivoox.com/20-crossover-linuxero-directo-sobre-gnome-plasma_md_19395669_wp_1.mp3" type="audio/mpeg">
</audio>


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://feedpress.me/podcastlinux>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.gitlab.io/Linux-Express/feed>
