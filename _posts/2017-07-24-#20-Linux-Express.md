---
title: "#20 Linux Express"
date: 2017-07-24
author: juan
category: linuxexpress
featimg: 2017/20LinuxExpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/%2320%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2017/20LinuxExpress.png)  
No cerramos en verano. Semanalmente alternamos el podcast normal con este Linux Express.

<audio controls>
  <source src="https://github.com/podcastlinux/podcastlinux.github.io/raw/master/Linux-Express/%2320%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>

Repasamos lo acontecido en estas últimas semanas del mes de julio:

+ [Episodio #30 Especial Maratón Linuxero](https://avpodcast.net/podcastlinux/especialmaratonlinuxero/)
+ Próximo episodio Especial [Tenerife LanParty 2017](https://tlp-tenerife.com/)
+ Probando [Ardour](https://ardour.org/)
+ Aprendiendo [Mixxx](https://www.mixxx.org/) con el curso de [Dj Mao Mix](https://www.youtube.com/watch?v=1aaaLKeIXiI&list=PLt_wY1uOEKnSj-Sebg9jku4hLAONMtxto)
+ Apúntate al [Maratón Linuxero](https://t.me/maratonlinuxero)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://feedpress.me/podcastlinux>
+ Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>
