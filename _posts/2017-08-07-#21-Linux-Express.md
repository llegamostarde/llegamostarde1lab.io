---
title: "#21 Linux Express"
author: juan
date: 2017-08-07
category: linuxexpress
featimg: 2017/21LinuxExpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/%2321%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2017/21LinuxExpress.png)  
Seguimos activos en verano. Semanalmente alternamos el podcast normal con este Linux Express y estas semanas han pasado muchas cosas.

<audio controls>
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/%2321%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>

Repasamos lo acontecido en esta primera semana del mes de agosto:

+ [Episodio #31 Especial TLP2017](https://avpodcast.net/podcastlinux/tlp2017/)
+ Próximo episodio Linux Connexion con [ReciclaNet](http://www.reciclanet.org/)
+ Sigo disfrutando de [Ardour](https://ardour.org/) con mi nueva interfaz de audio [Behringer UMC202HD](https://www.music-group.com/Categories/Behringer/Computer-Audio/Audio-Interfaces/UMC202HD/p/P0BJZ)
+ Apúntate al [Maratón Linuxero](https://t.me/maratonlinuxero)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://feedpress.me/podcastlinux>
+ Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>
