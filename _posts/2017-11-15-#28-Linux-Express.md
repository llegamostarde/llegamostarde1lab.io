---
title: "#28 Linux Express"
date: 2017-11-15
author: juan
category: [linuxexpress]
featimg: 2017/28linuxexpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/28linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2017/28linuxexpress.png)
Todos los miércoles, entrega de un podcast, ya sea Linux Express o Podcast Linux. 

<audio controls>
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/27linuxexpress.ogg" type="audio/ogg">
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/27linuxexpress.mp3" type="audio/mpeg">
</audio>

Estas semanas le he dado vueltas a estos temas:

+ [Episodio #38 Linux Connexion con Wikimedia España](http://avpodcast.net/podcastlinux/wikimedia).
+ Próximo episodio Smartphones y GNU/Linux
+ Curso: Crea tu propio podcast libre: [Archive.org](https://archive.org/details/@podcast_linux) y [Youtube](https://www.youtube.com/playlist?list=PLdt4gHTaH61HOOLsyAc2xYzdbinem9ooZ)
+ Encuesta Twitter sobre la mejor distro para novatos: [Encuesta](https://twitter.com/podcastlinux/status/930012099346132994)
+ Usando Firefox y [DuckDuckGo ](https://duckduckgo.com/)
+ Próximo [Maratón Linuxero ](https://maratonlinuxero.org/)

Las imagen del ordenador de sobremesa es propiedad de [Freepik.es](http://www.freepik.es/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.gitlab.io/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/podcastlinux>  
Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>  
