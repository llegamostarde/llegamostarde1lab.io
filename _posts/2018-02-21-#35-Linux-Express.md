---
title: "#35 Linux Express"
date: 2018-02-21
author: juan
category: [linuxexpress]
featimg: 2018/35linuxexpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/35linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2018/35linuxexpress.png)  
Un nuevo Linux Express, los audios de Telegram que se intercalan con los formales.  
<audio controls>
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/35linuxexpress.ogg" type="audio/ogg">
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/35linuxexpress.mp3" type="audio/mpeg">
</audio>

Esto es lo que ha sucedido en esta quincena:  
+ [Episodio #45 Distros Ligeras](http://avpodcast.net/podcastlinux/distrosligeras)
+ Siguiente episodio, Linux Connexion con [GALPon](https://www.galpon.org/).
+ YEPO 737A.
+ Nueva cuenta en [Mastodon](https://mastodon.social/@podcastlinux/)
+ KDE Neon vs. Linux Mint
+ Anímate a compartir tu [#EscritorioGNULinux](https://twitter.com/hashtag/escritoriognulinux)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/podcastlinux>  
Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>  
