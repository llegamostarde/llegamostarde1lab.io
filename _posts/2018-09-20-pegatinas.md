---
title: "Pegatinas Podcast Linux"
date: 2018-09-20
author: juan
category: [pegatinas]
featimg: 2018/pegatinas.png
podcast:
  audio: 
  video:
tags: [pegatinas]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2018/pegatinas.png)  
Varios oyentes se han puesto en contacto conmigo para poder hacer pegatinas.  

Especialmente quiero agradecer a [Víctor Asensio](https://twitter.com/victorsnk), fiel escuchante de Podcast linux, por ponerme en contacto con [WaterDrop HydroPrint](http://waterdrop.ga/).
Esta empresa especializada en Hidroimpresión, pintura y vinilos a la carta. Usan GIMP, Inkscape y DarkTable, aunque por obligaciones del Plotter, tienen que finalizar los trabajos con aplicaciones privativas.

Ellos directamente enviarán las pegatinas por correo. Por medio de su [Paypal](http://paypal.me/waterdrophydroprint) indicando: Nombre y apellidos, Dirección, contacto y tamaño y color de las pegatinas, te harán llegar a tu casa directamente las que desees.
![](https://podcastlinux.gitlab.io/media/compressed/2018/pegatinas2.png)  

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/podcastlinux>  
Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>  
