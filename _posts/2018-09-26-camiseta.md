---
title: "Camiseta Podcast Linux"
date: 2018-09-26
author: juan
category: [camiseta]
featimg: 2018/camiseta.png
podcast:
  audio: 
  video:
tags: [camiseta]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2018/camiseta.png)  
Con el arranque de la 3ª temporada, he querido realizar con el nuevo logo varias cosas.  

Recuerda que ya tienes las [pegatinas](https:/podcastlinux.com/pegatinas) gracias a [WaterDrop HydroPrint](http://waterdrop.ga/).
Ellos mismos te las envían a casa por un precio más que razonable.

También realicé en su momento un diseño para camisetas. La audiencia eligió una entre 4 posibles.  

![](https://podcastlinux.gitlab.io/media/compressed/2018/CamisetaPL.png)  
En esta ocasión no la vamos a distribuir por medio de una empresa. Voy a dejar el diseño en Gitlab.
Aquí lo tienes: <https://gitlab.com/podcastlinux/camiseta>

Que cada cual desgargue el diseño y lo imprima en su localidad. Así fomentamos el comercio local.

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/podcastlinux>  
Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>  
