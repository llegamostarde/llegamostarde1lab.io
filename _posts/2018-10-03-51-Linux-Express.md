---
title: "#51 Linux Express"
date: 2018-10-03
author: juan
category: [linuxexpress]
featimg: 2018/51linuxexpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/51linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2018/51linuxexpress.png)  
Atención, recuerda que tenemos nuevas empresas colaboradoras, [Chicles GUW](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga).  
<audio controls>
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/51linuxexpress.ogg" type="audio/ogg">
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/51linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ [Episodio [#61 Linux Connexion con RikyLinux](https://avpodcast.net/podcastlinux/rikylinux)
+ Próximo episodio: Especial [Jpod18](http://jpod.es/)
+ Gracias a las Empresas colaboradoras: [Chicles WUG](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga)
+ Actualización [KDE Neon](https://neon.kde.org/)
+ [System76](https://system76.com/) y su [Thelio](https://thel.io/) Open Source.
+ Diseños GNU/Linux en [Linux.pictures](https://linux.pictures/)
+ Territorio f-Droid: [Siete](https://f-droid.org/es/packages/de.baumann.sieben/)
+ Futuros programas.

Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [https://pixabay.com/photo-2322811/). Los logos WUG, Waterdrop Hydroprint y Jpod18Madrid son propiedad de sus respectivas empresas.


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/podcastlinux>  
Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>  
